package com.reportorder.btpnreadliness.exception;

public class NotFoundException extends Exception {

    public NotFoundException(String message) {
        super(message);
    }
}
