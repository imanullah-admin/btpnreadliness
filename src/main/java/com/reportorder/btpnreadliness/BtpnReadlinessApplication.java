package com.reportorder.btpnreadliness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtpnReadlinessApplication {

    public static void main(String[] args) {
        SpringApplication.run(BtpnReadlinessApplication.class, args);
    }

}
