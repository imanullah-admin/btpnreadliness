package com.reportorder.btpnreadliness.dto;

import lombok.*;

@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomersDto {

    private String customersAddress;

    private String customersCode;

    private String customersName;

    private String customersPhone;

    private String pic;
}
