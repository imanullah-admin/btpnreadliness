package com.reportorder.btpnreadliness.dto;

import lombok.*;

import java.math.BigDecimal;

@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemsDto {

    private String itemCode;

    private String itemName;

    private BigDecimal price;

    private BigDecimal stock;
}
