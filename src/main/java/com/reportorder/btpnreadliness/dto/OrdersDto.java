package com.reportorder.btpnreadliness.dto;

import lombok.*;

import java.math.BigDecimal;

@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrdersDto {

    private String orderCode;

    private BigDecimal quantity;

    private BigDecimal totalPrice;

    private Long customersId;

    private Long itemId;
}
