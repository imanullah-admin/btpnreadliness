package com.reportorder.btpnreadliness.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "items")
public class Items implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "items_id")
    private Long itemId;

    @Column(name = "items_code")
    private String itemCode;

    @Column(name = "is_available")
    private boolean isAvailable;

    @Column(name = "items_name")
    private String itemName;

    @Column(name = "last_re_stock")
    private Date lastReStock;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "stock")
    private BigDecimal stock;

}
