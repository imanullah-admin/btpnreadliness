package com.reportorder.btpnreadliness.model;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.sql.Date;

@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "customers")

public class Customers implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private Long customersId;

    @Column(name = "customer_address")
    private String customersAddress;

    @Column(name = "customer_code")
    private String customersCode;

    @Column(name = "customer_name")
    private String customersName;

    @Column(name = "customer_phone")
    private String customersPhone;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "last_order_date")
    private Date lastOrder;

    @Column(name = "pic")
    private String pic;
}
