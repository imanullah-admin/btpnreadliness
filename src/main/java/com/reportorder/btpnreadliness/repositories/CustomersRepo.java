package com.reportorder.btpnreadliness.repositories;


import com.reportorder.btpnreadliness.model.Customers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomersRepo extends JpaRepository<Customers, Long> {
}
