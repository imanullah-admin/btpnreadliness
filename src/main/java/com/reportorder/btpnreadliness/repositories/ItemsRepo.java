package com.reportorder.btpnreadliness.repositories;

import com.reportorder.btpnreadliness.model.Items;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemsRepo extends JpaRepository<Items, Long> {
}
