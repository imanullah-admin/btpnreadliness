package com.reportorder.btpnreadliness.repositories;

import com.reportorder.btpnreadliness.model.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrdersRepo extends JpaRepository<Orders, Long> {
}
