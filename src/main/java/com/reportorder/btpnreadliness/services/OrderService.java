package com.reportorder.btpnreadliness.services;

import com.reportorder.btpnreadliness.dto.OrdersDto;
import com.reportorder.btpnreadliness.exception.NotFoundException;
import com.reportorder.btpnreadliness.model.Orders;

import java.util.List;

public interface OrderService {

    List<Orders> getAllOrders() throws NotFoundException;

    Orders getOrderById(Long id) throws NotFoundException;

    Orders createOrder(OrdersDto ordersDto) throws NotFoundException;

    Orders updateOrder(Long id, Orders updateOrder) throws NotFoundException;

    void deleteOrderById(Long id) throws NotFoundException;
}
