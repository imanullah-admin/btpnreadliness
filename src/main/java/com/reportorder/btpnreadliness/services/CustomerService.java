package com.reportorder.btpnreadliness.services;

import com.reportorder.btpnreadliness.dto.CustomersDto;
import com.reportorder.btpnreadliness.exception.NotFoundException;
import com.reportorder.btpnreadliness.model.Customers;

import java.util.List;

public interface CustomerService {

    List<Customers> getAllCustomers() throws NotFoundException;

    Customers getCustomerById(Long id) throws NotFoundException;

    Customers createCustomer(CustomersDto customersDto);

    Customers updateCustomer(Long id, Customers customer) throws NotFoundException;

    void deleteCustomer(Long id) throws NotFoundException;
}
