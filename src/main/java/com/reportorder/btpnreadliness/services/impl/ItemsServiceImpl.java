package com.reportorder.btpnreadliness.services.impl;

import com.reportorder.btpnreadliness.dto.ItemsDto;
import com.reportorder.btpnreadliness.exception.NotFoundException;
import com.reportorder.btpnreadliness.model.Items;
import com.reportorder.btpnreadliness.repositories.ItemsRepo;
import com.reportorder.btpnreadliness.services.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class ItemsServiceImpl implements ItemService {

    private final ItemsRepo itemsRepository;

    @Override
    public List<Items> getAllItems() throws NotFoundException {
        List<Items> itemRepo = itemsRepository.findAll();
        if (itemRepo.isEmpty()) {
            throw new NotFoundException("We don't have any Item for now");
        } else {
            return itemRepo;
        }
    }

    @Override
    public Items getItemById(Long id) throws NotFoundException {
        Optional<Items> optionalItems = itemsRepository.findById(id);
        return optionalItems.orElseThrow(()-> new NotFoundException("Item ID " + id + " Not Found!"));
    }

    @Override
    public Items createItem(ItemsDto itemsDto) {
        Items items = Items.builder()
                .itemName(itemsDto.getItemName())
                .itemCode(itemsDto.getItemCode())
                .isAvailable(true)
                .stock(itemsDto.getStock())
                .price(itemsDto.getPrice())
                .lastReStock((Date) Date.from(Instant.now()))
                .build();
        itemsRepository.save(items);
        return items;
    }

    @Override
    public Items updateItem(Long id, Items items) throws NotFoundException {
        itemsRepository.findById(id).orElseThrow(()-> new NotFoundException("Item ID" + id + " Not Found!"));
        items.setItemId(id);
        if (items.getStock().equals(BigDecimal.ZERO)) {
            items.setAvailable(false);
        } else {
            items.setLastReStock((Date) Date.from(Instant.now()));
        }
        itemsRepository.save(items);
        return items;
    }

    @Override
    public void deleteItem(Long id) throws NotFoundException {
        if (itemsRepository.findById(id).isPresent()) {
            itemsRepository.deleteById(id);
        } else {
            throw new NotFoundException("Item ID " + id + " Not Found!");
        }
    }
}
