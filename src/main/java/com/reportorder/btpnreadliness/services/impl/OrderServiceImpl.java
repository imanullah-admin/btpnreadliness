package com.reportorder.btpnreadliness.services.impl;

import com.reportorder.btpnreadliness.dto.OrdersDto;
import com.reportorder.btpnreadliness.exception.NotFoundException;
import com.reportorder.btpnreadliness.model.Items;
import com.reportorder.btpnreadliness.model.Orders;
import com.reportorder.btpnreadliness.repositories.CustomersRepo;
import com.reportorder.btpnreadliness.repositories.ItemsRepo;
import com.reportorder.btpnreadliness.repositories.OrdersRepo;
import com.reportorder.btpnreadliness.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class OrderServiceImpl implements OrderService {

    private final OrdersRepo ordersRepository;
    private final CustomersRepo customersRepository;
    private final ItemsRepo itemsRepository;

    @Override
    public List<Orders> getAllOrders() throws NotFoundException {
        List<Orders> orders = ordersRepository.findAll();
        if (orders.isEmpty()) {
            throw new NotFoundException("We don't have any Order for now");
        } else {
            return orders;
        }
    }

    @Override
    public Orders getOrderById(Long id) throws NotFoundException {
        Optional<Orders> optionalOrders = ordersRepository.findById(id);
        return optionalOrders.orElseThrow(()-> new NotFoundException("Order ID " + id + " Not Found!"));
    }

    @Override
    public Orders createOrder(OrdersDto ordersDto) throws NotFoundException {
        Orders orders = Orders.builder()
                .order_date((Date) Date.from(Instant.now()))
                .orderCode(ordersDto.getOrderCode())
                .quantity(ordersDto.getQuantity())
                .totalPrice(ordersDto.getTotalPrice())
                .customersId(customersRepository.findById(ordersDto.getCustomersId()).orElseThrow(
                        ()-> new NotFoundException("Customer ID Not Found!")
                ))
                .itemId(itemsRepository.findById(ordersDto.getItemId()).orElseThrow(
                        ()-> new NotFoundException("Item ID Not Found!")
                )).build();
        if ((orders.getQuantity().subtract(itemsRepository.findById(ordersDto.getItemId()).orElseThrow(
                ()-> new NotFoundException("Item ID Not Found!")).getPrice()))
                .compareTo(BigDecimal.ZERO)<0) {
            throw new NotFoundException("Quantity is greater than stock!");
        }
        Items items = itemsRepository.findById(orders.getItemId().getItemId()).orElseThrow(()-> new NotFoundException("Item Not Found!"));
        items.setStock(items.getStock().subtract(orders.getQuantity()));
        itemsRepository.save(items);
        ordersRepository.save(orders);
        return orders;
    }

    @Override
    public Orders updateOrder(Long id, Orders updateOrder) throws NotFoundException {
        ordersRepository.findById(id).orElseThrow(
                ()-> new NotFoundException("Order ID Not Found!"));
        updateOrder.setOrderId(id);
        ordersRepository.save(updateOrder);
        return updateOrder;
    }

    @Override
    public void deleteOrderById(Long id) throws NotFoundException {
        if (ordersRepository.findById(id).isPresent()) {
            ordersRepository.deleteById(id);
        } else {
            throw new NotFoundException("Order ID " + id + " Not Found!");
        }
    }
}
