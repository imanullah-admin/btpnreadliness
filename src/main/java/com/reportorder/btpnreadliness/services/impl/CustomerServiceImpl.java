package com.reportorder.btpnreadliness.services.impl;

import com.reportorder.btpnreadliness.dto.CustomersDto;
import com.reportorder.btpnreadliness.exception.NotFoundException;
import com.reportorder.btpnreadliness.model.Customers;
import com.reportorder.btpnreadliness.repositories.CustomersRepo;
import com.reportorder.btpnreadliness.services.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomersRepo customerRepository;

    @Override
    public List<Customers> getAllCustomers() throws NotFoundException {

        List<Customers> repo = customerRepository.findAll();
        if (repo.isEmpty()) {
            throw new NotFoundException("We don't have any Customer for now");
        } else {
            return repo;
        }
    }

    @Override
    public Customers getCustomerById(Long id) throws NotFoundException {
        Optional<Customers> optionalCustomer = customerRepository.findById(id);
        return optionalCustomer.orElseThrow(()-> new NotFoundException("Customer ID " + id + " Not Found!"));
    }

    @Override
    public Customers createCustomer(CustomersDto customersDto) {

        Customers customers = Customers.builder()
                .customersAddress(customersDto.getCustomersAddress())
                .customersCode(customersDto.getCustomersCode())
                .pic(customersDto.getPic())
                .customersPhone(customersDto.getCustomersPhone())
                .isActive(true)
                .customersName(customersDto.getCustomersName()).build();
        customerRepository.save(customers);
        return customers;
    }

    @Override
    public Customers updateCustomer(Long id, Customers updatedCustomer) throws NotFoundException {
        if (customerRepository.existsById(id)) {
            updatedCustomer.setCustomersId(id);
            return customerRepository.save(updatedCustomer);
        } else { throw new NotFoundException("Customer ID " + id + " Not Found!"); }
    }

    @Override
    public void deleteCustomer(Long id) throws NotFoundException {
        if (customerRepository.findById(id).isEmpty()) {
            throw new NotFoundException("Customer ID " + id + " Not Found!");
        } else {
            customerRepository.deleteById(id);
        }
    }
}
