package com.reportorder.btpnreadliness.services;

import com.reportorder.btpnreadliness.dto.ItemsDto;
import com.reportorder.btpnreadliness.exception.NotFoundException;
import com.reportorder.btpnreadliness.model.Items;

import java.util.List;

public interface ItemService {

    List<Items> getAllItems() throws NotFoundException;

    Items getItemById(Long id) throws NotFoundException;

    Items createItem(ItemsDto itemsDto);

    Items updateItem(Long id, Items items) throws NotFoundException;

    void deleteItem(Long id) throws NotFoundException;
}
